![logo](https://cdn.radar-network.com/images/radarNetworkLogo/logo.svg)

# Radar Network <small>all Systems</small>

> A magical documentation of all Developer Interfaces Radar can offer.

- Simple and light as a feather
- Super duper fast
- Oh and it is fast

[<i class="fab fa-gitlab"></i> GitLab](https://gitlab.com/radarSystem)

![color](linear-gradient(#FFFFFF,#00d1ff))